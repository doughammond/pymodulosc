##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

import gobject


def getModulOSC():

	sess_bus = dbus.SessionBus()

	ModulOSC = sess_bus.get_object('com.bel.ModulOSC', '/')

	return ModulOSC


def testSig_handler():
	print("Test Signal!")

if __name__ == '__main__':
	ModulOSC = getModulOSC()

	ModulOSC.connect_to_signal('testSig', testSig_handler)

	ModulOSC.ping()

	loop = gobject.MainLoop()
	try:
		loop.run()
	except KeyboardInterrupt:
		loop.quit()
